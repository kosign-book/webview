//
//  TestViewController.swift
//  WebViewDemo
//
//  Created by kimlang on 1/12/23.
//

import UIKit
import WebKit

class TestViewController: UIViewController, WKNavigationDelegate {
    
    //Mark: -IBOutlet
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    //Run HTML
//    var htmlString = """
//    <html>
//<body>
//<h1>Hello World</h1>
//<a href="https://developer.apple.com/documentation/webkit/wkwebview">link text</a>
//</body>
//</html>
//"""
    //Mark: -LifeCyle
    override func viewDidLoad() {
        super.viewDidLoad()
        //for ddisplay func webView
        webView.navigationDelegate = self
        
//        Request by URL
              let url = URL(string: "https://developer.apple.com/documentation/webkit/wkwebview")
                let urlRequest = URLRequest(url: url!)
                webView.load(urlRequest)
                webView.goBack()
                webView.goForward()
        
        //Run HTML
        //        webView.loadHTMLString("<h1>Hello World</h1> ", baseURL: nil)
        //webView.loadHTMLString(htmlString, baseURL: nil)
    }
    
    
    @IBAction func goBackAction(_ sender: Any) {
        webView.goBack()
    }
    
    @IBAction func goForwardAction(_ sender: Any) {
        webView.goForward()
    }
    
    //Mark: -func webview
        func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
            print("Show didCommit")
            loading.isHidden = false
            loading.startAnimating()
        }
    
        func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            print("Show DidFinish")
            loading.isHidden = true
            loading.startAnimating()
        }
    
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("............................didFail withError............................")
        print(error)
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("............................didFailProvisionalNavigation............................")
        print(error)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        print("............................Webkit navigation response............................")
        decisionHandler(.allow)
    }

}
