//
//  CONTENT_LIST.swift
//  WebViewDemo
//
//  Created by Thorn Loemkimleak on 9/29/22.
//

import Foundation

enum CONTENT_TYPE {
    case InitWebView
    case LoadURL
    case WKNavigationDelegate
    case Cookie
    case LoadLocalHTML
    case LoadLocalHTMLString
    case LoadJavaScriptMethod
    case InjectJavaScriptCode
    case CallBackFromJavaScript
}

struct CONTENT {
    var title   : String?
    var type    : CONTENT_TYPE
}
