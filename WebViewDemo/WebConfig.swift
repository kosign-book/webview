//
//  WebConfig.swift
//  WebViewDemo
//
//  Created by kimlang on 1/17/23.
//

import UIKit
import WebKit

class WebConfig: UIViewController, WKUIDelegate {

    var webView: WKWebView!
        
        override func loadView() {
            let webConfiguration = WKWebViewConfiguration()
            webView = WKWebView(frame: .zero, configuration: webConfiguration)
            webView.uiDelegate = self
            view = webView
        }

        override func viewDidLoad() {
            super.viewDidLoad()
            
            let myURL = URL(string:"https://www.apple.com")
            let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)
        }

    
}
